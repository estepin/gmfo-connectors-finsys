<?php
namespace MfoRu\MfoAccounting\Finsys;

use MfoRu\Contracts\MfoAccounting\Exceptions\NotAuth;
use MfoRu\Contracts\MfoAccounting\Exceptions\UnknownResponse;
use MfoRu\Contracts\MfoAccounting\Exceptions\WrongConfig;
use SoapClient;

class Api
{
    protected $soapClient;
    protected $exchangeKey;

    protected $debugData;

    function __construct($wsdlUrl, $login, $password, $exchangeKey)
    {
        $this->exchangeKey = $exchangeKey;
        try{
@            $this->soapClient = new SoapClient(
                $wsdlUrl,
                [
                    'login' => $login,
                    'password' => $password,
                    'trace' => true,
                    'soap_version' => SOAP_1_1
                ]
            );
            $this->debug();
        }catch (\SoapFault $e) {
            $message = false;
            if($e->faultcode == 'WSDL')
                $message = 'WSDL не корректен или отсутствует по адресу '.$wsdlUrl;

            throw new WrongConfig($message);
        }

    }


    function getOrgs()
    {
        return $this->getLists('Справочник.ПодразделенияОрганизаций');
    }

    function getCountries()
    {
        return $this->getLists('Справочник.СтраныМира');
    }

    function getDocsTypes()
    {
        return $this->getLists('Справочник.ВидыДокументовФизическихЛиц');
    }

    function getLists($metaName)
    {
        try {
            $res = $this->soapClient->GetLists((object)
            [
                'ExchangeKey' => $this->exchangeKey,
                'Lists' => [
                    'Value' => $this->getValueStruct(
                        [
                            'MetadataName' => $metaName,
                            'Params' => ''
                        ]
                    )
                ]
            ]
            );
            $this->debug();
        } catch (\SoapFault $e) {
            if($e->getMessage() == 'Unauthorized') {
                $this->debug();
                throw new NotAuth();
            }
        } catch (\Exception $e) {
            $this->debug();
            throw $e;
        }

        if($res->return == false && $res->ResultMessage == 'Переданный ключ обмена не соответствует сохраненному в системе.')
        {
            $this->debug();
            throw new WrongConfig($res->ResultMessage);
        }

        return $res;
    }

    function addRequest($data)
    {
        try {
            $res = $this->soapClient->LoadRequests(
                (object) array(
                    'ExchangeKey' => $this->exchangeKey,
                    'Data' => $data
                )
            );
            $this->debug();
        } catch (\Exception $e) {
            $this->debug();
            throw new UnknownResponse();
        }

        return $res;
    }

    function debug()
    {
        $this->debugData['request_body'] = $this->soapClient->__getLastRequest();
        $this->debugData['response_body'] = $this->soapClient->__getLastResponse();
    }

    function getValueStruct($data)
    {
        $xmlArr = [];
        foreach($data as $k => $v)
        {
            $xmlArr[] = $this->getPropertyXml($k, $v);
        }

        $xml = '<Value xsi:type="Structure" xmlns="http://v8.1c.ru/8.1/data/core">'.implode('', $xmlArr).'</Value>';

        $lists = new \SoapVar($xml, XSD_ANYXML, 'Value', 'http://www.w3.org/2001/XMLSchema-instance');

        return $lists;
    }

    function getPropertyXml($propName, $propValue)
    {
        return '<Property name="'.$propName.'"><Value>'.$propValue.'</Value></Property>';
    }

    function getSoapVar($xmlStr)
    {
        return new \SoapVar($xmlStr, XSD_ANYXML);
    }

    function getProp($propName, $propValue)
    {
        $xmlStr = $this->getPropertyXml($propName, $propValue);
        return $this->getSoapVar($xmlStr);
    }

    function getDebugData()
    {
        return $this->debugData;
    }

}