<?php
namespace MfoRu\MfoAccounting\Finsys;

class Config
{
    public $wsdlUrl;
    public $login;
    public $password;
    public $exchangeKey;

    public $creditProductDefault;
    public $creditPurposeDefault;
    public $orgNameDefault;
    public $orgUnitDefault;

}