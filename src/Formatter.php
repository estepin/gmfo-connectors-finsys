<?php
namespace MfoRu\MfoAccounting\Finsys;

use MfoRu\Contracts\MfoAccounting\Anket;
use MfoRu\Contracts\MfoAccounting\Exceptions\UnknownResponse;

class Formatter
{
    protected $apiClient;
    protected $config;

    function __construct(Api $apiClient, Config $config)
    {
        $this->apiClient = $apiClient;
        $this->config = $config;
    }

    /*
     * Конвертирует Anket в массив подходящий для АПИ
     */
    function anketToAnketArray(Anket $anket, $dateRequest = false)
    {
        $dictDocs = $this->getDocTypes();
        $passRfId = $this->findPassRf($dictDocs);
        $rfId = 643;

        $pass = $this->passSeriaNum($anket);

        if (!$dateRequest)
        {
            $dateTimeRequest = date('Y-m-d\TH:i:s');
            $dateRequest = date('Y-m-d');
        }


        if ($anket->gender == '1')
            $gender = 'Мужской';
        elseif ($anket->gender == '2')
            $gender = 'Женский';
        else
            throw new \Exception('Wrong gender value');


        $result = [
            'ЗаявкаНаЗайм' => [
                'ID' => '',
                'Posted' => true,
                'Организация' => $this->config->orgNameDefault,
                'Подразделение' => $this->config->orgUnitDefault,
                'ДатаПодачи' => $dateTimeRequest,
                'Заявитель' => [
                    'ID' => false,
                    'Фамилия' => $anket->lastname,
                    'Имя' => $anket->firstname,
                    'Отчество' => $anket->middlename,
                    'ДатаРождения' => $anket->birthdate,
                    'МестоРождения' => $anket->passBirthPlace,
                    'Гражданство' => $rfId,
                    'Пол' => $gender,
                    'Addresses' => [
                        [
                            'Вид' => 'ФактАдрес',
                            'Страна' => $rfId,
                            'Индекс' => $anket->liveIndex,
                            'Регион' => $anket->liveRegion,
                            'Город' => $anket->liveCity,
                            'Улица' => $anket->liveStreet,
                            'Дом' => $anket->liveHouse,
                            'Строение' => $anket->liveBuild,
                            'Квартира' => $anket->liveRoom
                        ],
                        [
                            'Вид' => 'ЮрАдрес',
                            'Страна' => $rfId,
                            'Индекс' => $anket->liveIndex,
                            'Регион' => $anket->liveRegion,
                            'Город' => $anket->liveCity,
                            'Улица' => $anket->liveStreet,
                            'Дом' => $anket->liveHouse,
                            'Строение' => $anket->liveBuild,
                            'Квартира' => $anket->liveRoom
                        ]
                    ],
                    'PhoneNumbers' => [
                        [
                            'Вид' => 'Мобильный',
                            'Номер' => $anket->phone
                        ]
                    ],
                    'EMails' => [$anket->email],
                    'Credentials' => [
                        [
                            'ВидДокумента' => $passRfId,
                            'Серия' => $pass['seria'],
                            'Номер' => $pass['num'],
                            'КемВыдан' => $anket->passWhoIssued,
                            'КодПодразделения' => $anket->passCode,
                            'ЯвляетсяДокументомУдостоверяющимЛичность' => 'true',
                            'ДатаВыдачи' => $anket->passDate,

                        ]
                    ]
                ],
                'ДатаВыдачи' => $dateRequest,
                'КредитныйПродукт' => $this->config->creditProductDefault,
                'Сумма' => $anket->summ,
                'Срок' => $anket->term,
                'НазначениеЗайма' => $this->config->creditPurposeDefault
            ]
        ];

        return $result;
    }

    /*
     * Конвертирует ответ метода GetList от апи в удобный массив
     */
    function apiListToArray($data)
    {
        $arrData = $data->ResultDetails->Value->enc_value->Property[1]->Value->enc_value->Value;

        foreach ($arrData as $item) {
            $prop = $item->enc_value->Property;
            foreach ($prop as $v) {
                if ($v->name == 'Presentation' || $v->name == 'MetaDataName')
                    $name = $v->Value;

                if ($v->name == 'ID')
                    $id = $v->Value;
            }

            $result[$id] = $name;
        }

        return $result;

    }

    function apiResultRequest($data)
    {
        if ($data->return == true && $data->ResultDetails->Результат->РезультатЗагрузки === 1)
            return $data->ResultDetails->Результат->IDЗаявки;
        else
            throw new UnknownResponse();
    }

    function findPassRf($docTypes)
    {
        $values = [
            'Паспорт гражданина РФ'
        ];

        foreach ($values as $val) {
            $key = array_search($val, $docTypes);
            if ($key !== false)
                return $key;
        }

        return false;
    }

    function findRfId($countries)
    {
        $countries = $this->getCountries();

        var_dump($countries);
    }

    protected function getDocTypes()
    {
        $apiResultData = $this->apiClient->getDocsTypes();
        return $this->apiListToArray($apiResultData);
    }

    protected function getCountries()
    {
        $apiResultData = $this->apiClient->getCountries();
        return $this->apiListToArray($apiResultData);
    }

    function passSeriaNum(Anket $anket)
    {
        $ex = explode(' ', $anket->passSeriaNum);
        $pass['seria'] = implode(' ', [$ex[0], $ex[1]]);
        $pass['num'] = $ex[2];

        return $pass;
    }
}