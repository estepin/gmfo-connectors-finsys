<?php
namespace MfoRu\MfoAccounting\Finsys;

use MfoRu\Contracts\MfoAccounting\Anket;
use MfoRu\Contracts\MfoAccounting\Connector as BaseInterface;
use MfoRu\Contracts\MfoAccounting\DebugData as DebugDataInterface;
use MfoRu\Contracts\MfoAccounting\Exceptions\UnknownResponse;

class Connector implements BaseInterface
{
    protected $debugMode;
    protected $debugData;

    function addToUpload(Anket $anket, $config)
    {
        $apiClient = $this->apiFromConfig($config);
        $formatter = new Formatter($apiClient, $config);

        try{
            $anketDataArray = $formatter->anketToAnketArray($anket);
            $resultRequest = $apiClient->addRequest($anketDataArray);
            return $formatter->apiResultRequest($resultRequest);
        }catch (\Exception $e) {
            $apiDebugData = $apiClient->getDebugData();
            $this->debugData = new DebugData($apiDebugData);
            throw $e;
        }
    }

    function checkStatus($anketId, $config)
    {
        throw new \Exception('Method is not implemented yet');
    }

    function getAccData($id)
    {
        // TODO: Implement getAccData() method.
    }

    function getConfigModel()
    {
        return new Config();
    }

    function getDebugData():DebugDataInterface
    {
        return $this->debugData;
    }

    function testConfig($config): bool
    {
        $anket = $this->getTestAnket();
        return (bool)$this->addToUpload($anket, $config);
    }

    protected function apiFromConfig($config)
    {
        return new Api($config->wsdlUrl, $config->login, $config->password, $config->exchangeKey);
    }

    function getTestAnket()
    {
        $anket = new Anket();

        $anket->summ = '1000';
        $anket->term = '5';

        $anket->lastname = 'Петров';
        $anket->firstname = 'Петр';
        $anket->middlename = 'Петрович';
        $anket->gender = '1';
        $anket->birthdate = '1990-01-01';

        $anket->email = 'test@mfo.ru';
        $anket->phone = '+79091112233';

        $anket->passSeriaNum = '11 26 123321';
        $anket->passWhoIssued = 'УВД';
        $anket->passCode = '123-321';
        $anket->passDate = '2011-01-01';
        $anket->passBirthPlace = 'Саратов';

        $anket->regIndex = '410001';
        $anket->regRegion = 'Саратовская обл';
        $anket->regCity = 'Саратов';
        $anket->regStreet = 'Московская';
        $anket->regHouse = '1';
        $anket->regRoom = 1;

        $anket->liveIndex = '410001';
        $anket->liveRegion = 'Саратовская обл';
        $anket->liveCity = 'Саратов';
        $anket->liveStreet = 'Московская';
        $anket->liveHouse = '1';
        $anket->liveRoom = 1;

        return $anket;
    }

    function enableDebugMode()
    {
        $this->debugMode = true;
    }

    function disableDebugMode()
    {
        $this->debugMode = false;
    }
}